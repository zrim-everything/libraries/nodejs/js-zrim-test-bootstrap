# Simple Test Launcher

```javascript
require('zrim-test-bootstrap').SimpleTestLauncher
```

## Introduction

A simple class to launch the test. It is in charge of starting jasmine and istanbul.

## Usage

```javascript
const launcher = new SimpleTestLauncher();

const config = {}; // You may use the builder

launcher.configure(config)
.then(() => launcher.run())
.catch(error => {
  // Something went wrong and test failed to launch
});

// The run will automatically call process.exit()
```

## Configuration

Configuration:
- rootDirectory *string*: The root directory
- project *Project* : The project configuration
  - rootDirectoryPath *string* : The project path
- test *Test* : The test configuration
  - type *string* : The test type
  - specDirPath *string|undefined* : The spec directory path
  - specFileFilters *Array<string|RegExp|Function>* : Some filter
  - jasmine *object|undefined* : Jasmine specific configuration
    - features *object|undefined* : The features
      - enableJunitXml *boolean|undefined* : Enabled or not the junit reports
      - enableHtml *boolean|undefined* : Enable or not the html reports
    - nativeConfig *object|undefined* : Possible native configuration
- reports *Reports* : The reports configuration
  - rootDirectoryPath *string* : The root path for reports
- coverage *Coverage* : The coverage configuration
  - enabled *boolean* : Enable or not the coverage
  - summary *object|undefined* : The summary config
    - enabled *boolean* : Enable or not the summary
    - template *string* : The template to use
- steps *Steps* : The steps configuration
  - preLaunch *object[]* : The steps before coverage or test has been initialized
    - handler *Function* : The function to execute
  - preTestLaunch *object[]* : The steps before the tests start (Coverage has been started)
    - handler *Function* : The function to execute
  - postExecution *object[]* : The steps after the test finished (with success or not)
    - handler *Function* : The function to execute
  - cleanUp *object[]* : Clean up steps. Always executed
    - handler *Function* : The function to execute

### Summary template

The summary use a `lodash` template with `{{ }}` as interpolate. Use the `<% %>` to use 
javascript code.

Variables:
- total *Object*
  - percent *Number* : The total percentage of coverage done
  - total *Number* : The total point coverage
  - covered *Number* : The number of point covered
  - skipped *Number* : The number of point ignored
- lines *Object* : Line coverage
  - percent *Number* : The percentage of coverage done
  - total *Number* : The total point coverage
  - covered *Number* : The number of point covered
  - skipped *Number* : The number of point ignored
- statements *Object* : Statement coverage
  - percent *Number* : The percentage of coverage done
  - total *Number* : The total point coverage
  - covered *Number* : The number of point covered
  - skipped *Number* : The number of point ignored
- functions *Object* : Function coverage
  - percent *Number* : The percentage of coverage done
  - total *Number* : The total point coverage
  - covered *Number* : The number of point covered
  - skipped *Number* : The number of point ignored
- branches *Object* : Branch coverage
  - percent *Number* : The percentage of coverage done
  - total *Number* : The total point coverage
  - covered *Number* : The number of point covered
  - skipped *Number* : The number of point ignored

### Steps

The step require a handler with the definition:
````
(context: BaseStepHandlerContext) => Promise<StepHandlerOnResolve | null | undefined>
````

Context:
- launcher *TestLauncher*: The launcher
- logger *Logger* : A logger
- executionContext *object* : An object accessible during the test

PostExecution *Context*: 
- testSucceed *boolean* : If the test succeed or not

Response
- stopWorkflow *boolean|undefined* : Stop the steps workflow or not

### Post execution

The post execution context will contains the additional properties:
- testSucceed *Boolean*: Tells you if the test finished with success or not

## Methods

### configure

```
initialize(config: Object): Promise<void>
```

Configure the launcher

### run

```
run(): Promise<void>
```

Start the tests. This will call :
- The steps
- The coverage if enabled
- The tests

It will call process.exit at the end of the process with 0 if all succeed, otherwise something
different than 0.

### execute (static)

```typescript
execute(config);
```

Execute the test in background

## Environment Variable

The launcher allow you to define environment variable to change the configuration.

- ZE_DISABLE_CODE_COVERAGE : If defined, it will disable the coverage.

## Debug

We use [debug](https://www.npmjs.com/package/debug) with the prefix `ze:test:launcher`.
