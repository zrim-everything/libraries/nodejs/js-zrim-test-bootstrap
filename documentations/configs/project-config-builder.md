# Project Configuration Builder

```javascript
require('zrim-test-bootstrap').configs.SimpleProjectConfigBuilder
```

```typescript
import {configs} from 'zrim-test-bootstrap';
import SimpleProjectConfigBuilder = configs.SimpleProjectConfigBuilder;
```

## Introduction

Help to generate the configuration for the project

## Methods

### construct

The constructor may receive a options:
- parent [SimpleTestLauncherConfigBuilder](test-launcher-config-builder.md) : 
The parent configuration builder

### clear

```
clear() : SimpleProjectConfigBuilder
```

Clear the configuration

### parentBuilder

```
parentBuilder() : SimpleTestLauncherConfigBuilder
```

Returns the [parent builder](test-launcher-config-builder.md)

The value returned is the one given to the constructor.

### build

```
build(): Object
```

Generate the configuration

### rootDirectoryPath

```
rootDirectoryPath(path: string) : SimpleProjectConfigBuilder
```

Set the root directory of the project
