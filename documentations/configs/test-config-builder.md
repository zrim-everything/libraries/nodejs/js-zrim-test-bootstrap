# Test Configuration Builder

```javascript
require('zrim-test-bootstrap').configs.SimpleTestConfigBuilder
```

```typescript
import {configs} from 'zrim-test-bootstrap';
import SimpleTestConfigBuilder = configs.SimpleTestConfigBuilder;
```

## Introduction

Help to generate the configuration for the test

## Methods

### construct

The constructor may receive a options:
- parent [SimpleTestLauncherConfigBuilder](test-launcher-config-builder.md) : 
The parent configuration builder

### clear

```
clear() : SimpleTestConfigBuilder
```

Clear the configuration

### parentBuilder

```
parentBuilder() : SimpleTestLauncherConfigBuilder
```

Returns the [parent builder](test-launcher-config-builder.md)

The value returned is the one given to the constructor.

### build

```
build(): Object
```

Generate the configuration

### specDirPath

```
specDirPath(path: string) : SimpleTestConfigBuilder
```

Set the spec directory path

### type

```
type(value: string) : SimpleTestConfigBuilder
```

Set the test type

### unitTest

```
unitTest(): SimpleTestConfigBuilder
```

Set the type to unit. Equivalent of `type('unit')`

### integrationTest

```
integrationTest(): SimpleTestConfigBuilder
```

Set the type to integration. Equivalent of `type('integration')`

### systemTest

```
systemTest(): SimpleTestConfigBuilder
```

Set the type to system. Equivalent of `type('system')`

### nativeConfiguration

```
nativeConfiguration(value: Object): SimpleTestConfigBuilder
```

Set a native configuration to be added with the generated one

### withSpecFileFilter

```
withSpecFileFilter(value: Function|string|RegExp): SimpleTestConfigBuilder
```

Add a new spec file filter
