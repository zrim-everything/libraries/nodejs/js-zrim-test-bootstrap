# Reports Configuration Builder

```javascript
require('zrim-test-bootstrap').configs.SimpleReportsConfigBuilder
```

```typescript
import {configs} from 'zrim-test-bootstrap';
import SimpleReportsConfigBuilder = configs.SimpleReportsConfigBuilder;
```

## Introduction

Help to generate the configuration for the reports

## Methods

### construct

The constructor may receive a options:
- parent [SimpleTestLauncherConfigBuilder](test-launcher-config-builder.md) : 
The parent configuration builder

### clear

```
clear() : SimpleReportsConfigBuilder
```

Clear the configuration

### parentBuilder

```
parentBuilder() : SimpleTestLauncherConfigBuilder
```

Returns the [parent builder](test-launcher-config-builder.md)

The value returned is the one given to the constructor.

### build

```
build(): Object
```

Generate the configuration

### rootDirectoryPath

```
rootDirectoryPath(path: string) : SimpleReportsConfigBuilder
```

Set the root directory where to put the reports
