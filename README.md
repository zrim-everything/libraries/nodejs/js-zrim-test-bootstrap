# Zrim Test Bootstrap

## Introduction

The goal of this project is to provide utilities to make test easier.

It allow to have an index file where to always start the test, this is usefull to prepare
variable for test.

See [SimpleTestLauncher](documentations/simple-test-launcher.md) for the launcher and
[configs](documentations/configs/README.md) for configuration helpers.

## Examples

### Unit Test

In this example, we use the configuration builder to help us create the configuration
with a nicer look.

And in case of success we start the test. The coverage and test result will automatically be saved.

```typescript
import {SimpleTestLauncher, configs} from 'zrim-test-bootstrap';
import * as pathUtils from 'path';

const configBuilder = new configs.SimpleTestLauncherConfigBuilder();
const rootDirectoryPath = pathUtils.resolve(__dirname + '/../../..');

// You are now in PROJECT_DIR/src/test/unit/index.js

configBuilder
  .projectConfiguration()
  .rootDirectoryPath(rootDirectoryPath)
  .parentBuilder()
  .coverageConfiguration()
  .enableSummary(true)
  .parentBuilder()
  .testConfiguration()
  .unitTest()
  .specDirPath(pathUtils.relative(rootDirectoryPath, __dirname))
  .parentBuilder()
  .withPreLaunchStep(async function (context) {
    // So something before istanbul is launch
  })
  .withPreTestLaunchStep(async function (context) {
    // So something before jasmine is launch and after istanbul is started
  })
  .withCleanUpStep(async function (context) {
    // Do some cleanup
  });

SimpleTestLauncher.execute(configBuilder.build());
```

The reports will automatically created to ROOT_PROJECT/reports/test/unit
