import * as createDebug from 'debug';

export interface Logger {
  debug(...args: any[]);
  info(...args: any[]);
  warning(...args: any[]);
  error(...args: any[]);
}

export function createLogger(name): Logger {
  return {
    debug: createDebug(`ze-test:${name}:debug`),
    info: createDebug(`ze-test:${name}:info`),
    warning: createDebug(`ze-test:${name}:warning`),
    error: createDebug(`ze-test:${name}:error`)
  };
}


