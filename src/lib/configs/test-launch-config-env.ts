import * as _ from 'lodash';
import Ajv = require("ajv");
import {testLauncher} from "../test-launcher";


namespace simpleTestLauncherConfigFromEnvUtils {

  export interface Properties {
    ajv: Ajv.Ajv;
  }
}

export class SimpleTestLauncherConfigFromEnvUtils {

  protected properties: simpleTestLauncherConfigFromEnvUtils.Properties;

  constructor() {
    this.properties = {
      ajv: new Ajv({
        coerceTypes: "array",
        removeAdditional: false,
        useDefaults: true
      })
    };

    this.properties.ajv.addSchema(this.getConfigurationSchema(), 'config');
  }

  /**
   * Generate the configuration from the environment variable
   */
  public generateConfiguration(): testLauncher.configuration.Configuration {
    const rawConfig = this.retrieveConfigurationFromEnvironments();
    const validatedConfiguration = this.validateRawConfiguration(rawConfig);

    return validatedConfiguration as testLauncher.configuration.Configuration;
  }

  /**
   * Returns the configuration schema to validate the config
   */
  protected getConfigurationSchema(): object {
    return {
      type: 'object',
      properties: {
        project: {
          type: 'object',
          properties: {
            rootDirectoryPath: {
              type: ['string', 'null']
            }
          }
        },
        reports: {
          type: 'object',
          properties: {
            rootDirectoryPath: {
              type: ['string', 'null']
            }
          }
        },
        coverage: {
          type: 'object',
          properties: {
            enabled: {
              type: ['boolean', 'null']
            },
            summary: {
              type: 'object',
              properties: {
                enabled: {
                  type: ['boolean', 'null']
                },
                template: {
                  type: ['string', 'null']
                }
              }
            }
          }
        },
        test: {
          type: 'object',
          properties: {
            type: {
              type: ['string', 'null']
            },
            jasmine: {
              type: 'object',
              properties: {
                features: {
                  type: 'object',
                  properties: {
                    enableJunitXml: {
                      type: ['boolean', 'null']
                    },
                    enableHtml: {
                      type: ['boolean', 'null']
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
  }

  /**
   * Validate And convert the raw input configuration
   * @param config The raw configuration
   */
  protected validateRawConfiguration(config: object): object {
    const convertedObject = _.cloneDeep(config);
    this.properties.ajv.validate('config', convertedObject);
    return convertedObject;
  }

  /**
   * Retrieve the raw configuration from the environment variables
   */
  protected retrieveConfigurationFromEnvironments(): object {
    const inputConfiguration = {
      project: {
        rootDirectoryPath: _.get(process.env, 'ZE_TEST_CONFIG_PROJECT_ROOT_DIR_PATH', undefined)
      },
      reports: {
        rootDirectoryPath: _.get(process.env, 'ZE_TEST_CONFIG_REPORTS_ROOT_DIR_PATH', undefined)
      },
      coverage: {
        enabled: _.get(process.env, 'ZE_TEST_CONFIG_COVERAGE_ENABLED', undefined),
        summary: {
          enabled: _.get(process.env, 'ZE_TEST_CONFIG_COVERAGE_SUMMARY_ENABLED', undefined),
          template: _.get(process.env, 'ZE_TEST_CONFIG_COVERAGE_SUMMARY_TEMPLATE', undefined)
        }
      },
      test: {
        type: _.get(process.env, 'ZE_TEST_CONFIG_TEST_TYPE', undefined),
        jasmine: {
          features: {
            enableJunitXml: _.get(process.env, 'ZE_TEST_CONFIG_TEST_ENABLE_REPORT_JUNIT', undefined),
            enableHtml: _.get(process.env, 'ZE_TEST_CONFIG_TEST_ENABLE_REPORT_HTML', undefined)
          }
        }
      }
    };

    return inputConfiguration;
  }
}
