import {SimpleTestLauncherConfigBuilder} from './test-launcher-config-builder';
import * as _ from 'lodash';
import {testLauncher} from "../test-launcher";

export namespace simpleReportsConfigBuilder {

  export interface Properties {
    parent: SimpleTestLauncherConfigBuilder | undefined;
    configuration: Configuration;
  }

  export interface Configuration {
    rootDirectoryPath: string | undefined;
  }
}

export class SimpleReportsConfigBuilder {

  protected properties: simpleReportsConfigBuilder.Properties;

  constructor(parent?: SimpleTestLauncherConfigBuilder) {
    this.properties = {
      parent,
      configuration: this._defaultConfiguration()
    };
  }

  /**
   * The configuration group name
   */
  public get _subConfigurationGroupName(): string {
    return 'reports';
  }

  /**
   * Clear the configuration
   */
  public clear(): SimpleReportsConfigBuilder {
    this.properties.configuration = this._defaultConfiguration();
    return this;
  }

  /**
   * Set the root directory
   * @param [value] The new value. Set undefined to set put the default value
   */
  public rootDirectoryPath(value?: string): SimpleReportsConfigBuilder {
    if (_.isNil(value)) {
      this.properties.configuration.rootDirectoryPath = this._defaultConfiguration().rootDirectoryPath;
    } else if (_.isString(value)) {
      this.properties.configuration.rootDirectoryPath = value;
    }

    return this;
  }

  /**
   * Returns the parent builder
   */
  public parentBuilder(): SimpleTestLauncherConfigBuilder {
    return this.properties.parent;
  }

  /**
   * Generate the object configuration
   */
  public build(): testLauncher.configuration.reports.Configuration {
    const userConfig = {
      rootDirectoryPath: this.properties.configuration.rootDirectoryPath
    };

    return _.merge({}, userConfig);
  }

  /**
   * Returns the default configuration
   */
  protected _defaultConfiguration(): simpleReportsConfigBuilder.Configuration {
    return {
      rootDirectoryPath: undefined
    };
  }
}
