

export namespace coverageRunner {

  export namespace configuration {

    export interface Configuration {
      /**
       * The project path
       */
      projectDirectoryPath: string;

      /**
       * The report path
       */
      reportDirectoryPath: string;
    }
  }

  export interface GenerateSummaryOptions {
    template: string;
  }
}

export interface CoverageRunner {

  /**
   * Initialize the coverage
   * @param options
   */
  initialize(options: coverageRunner.configuration.Configuration): Promise<void>;

  /**
   * Start the coverage
   */
  start();

  /**
   * Clean the coverage cache data
   */
  clean(): Promise<void>;

  /**
   * Save the reports
   */
  saveReports(): Promise<void>;

  /**
   * Generate the summary
   * @param options The options
   */
  generateSummary(options: coverageRunner.GenerateSummaryOptions): Promise<string>;
}
