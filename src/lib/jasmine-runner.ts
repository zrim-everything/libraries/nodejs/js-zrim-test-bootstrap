export namespace jasmineRunner {
  export interface StartOptions {
    executionContext: object;
  }

  export interface TestSpecFileFilterOnResolve {
    ignore: boolean | undefined | null;
  }

  /**
   * Spec filter
   */
  export type TestSpecFileFilter = (filePath: string) => Promise<TestSpecFileFilterOnResolve | null | undefined>;

  export interface Configuration {
    /**
     * The project path
     */
    projectDirectoryPath: string;

    /**
     * The spec directory path
     */
    specDirPath: string;

    /**
     * Define some filter to apply to each file. When using regexp, if match, the file is ignored
     */
    specFileFilters?: Array<string|TestSpecFileFilter|RegExp>;

    /**
     * The report path
     */
    reportDirectoryPath: string;

    /**
     * Features
     */
    features?: ConfigurationFeatures;
  }

  export interface ConfigurationReportFeature {
    /**
     * true to enable the report as junit xml
     */
    enableJunitXml?: boolean | undefined;
    /**
     * true to enable the report as HTML
     */
    enableHtml?: boolean | undefined;
  }

  export interface ConfigurationFeatures {
    /**
     * The report
     */
    reports?: ConfigurationReportFeature | undefined;
  }


  export namespace events {

    export interface Finished {
      _origin: JasmineRunner;
      success: boolean;
    }
  }
}

export interface JasmineRunner {

  /**
   * The last execution if succeed or not
   */
  readonly lastExecutionSucceed: boolean;

  /**
   * Initialize the runner
   * @param configuration The ru
   * Start the procnner configuration
   */
  initialize(configuration: jasmineRunner.Configuration): Promise<void>;

  /**
   * Start the process
   *
   * Wait for event Finished
   * @param options The options
   */
  start(options: jasmineRunner.StartOptions): Promise<void>;

  /**
   * Start and wait the end
   * @param options The options
   */
  run(options: jasmineRunner.StartOptions): Promise<jasmineRunner.events.Finished>;
}
