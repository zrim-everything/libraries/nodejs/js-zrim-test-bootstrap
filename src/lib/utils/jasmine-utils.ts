import {classUtils} from './class-utils';


// FROM : https://itnext.io/better-typescript-support-for-jasmine-20dc7454ba94

// tslint:disable
type Type<T> = {
  new (...args: any[]): T;
};
// tslint:enable

export function spyOnClass<T>(spiedClass: Type<T>): jasmine.SpyObj<T> {
  const prototype = spiedClass.prototype;

  // TODO : Check, may inheritance not have methods
  const methods = classUtils.getInstanceMethodNames(prototype);

  // return spy object
  return jasmine.createSpyObj('spy', [...methods]);
}

export function provideMock<T>(spiedClass: Type<T>) {
  return {
    provide: spiedClass,
    useValue: spyOnClass(spiedClass)
  };
}

