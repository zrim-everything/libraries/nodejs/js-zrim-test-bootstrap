import * as _ from 'lodash';

export interface ProcessTemplateOptions {
  /**
   * The template
   */
  template: string;
  /**
   * The variables
   */
  variables: object;
}

export interface ProcessTemplateOnResolve {
  /**
   * The data generated
   */
  data: string;
}

export async function processTemplate(options: ProcessTemplateOptions): Promise<ProcessTemplateOnResolve> {
  // Compile

  const compileOptions = {
    interpolate: /{{([\s\S]+?)}}/g,
    imports: {
      _: _.runInContext()
    }
  };
  const generate = _.template(options.template, compileOptions);

  // Run
  const data = generate(options.variables);
  return {
    data
  };
}
