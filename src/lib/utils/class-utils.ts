
// See http://code.fitness/post/2016/01/javascript-enumerate-methods.html

export class ClassUtils {

  public hasMethod(value: any, name: string) {
    const desc = Reflect.getOwnPropertyDescriptor(value, name);
    return !!desc && typeof desc.value === 'function';
  }

  public getInstanceMethodNames(value: any, stop?: object) {
    const names = [];

    if (typeof value === 'object' || typeof value === 'function') {
      // First get own properties
      Object.getOwnPropertyNames(value)
        .forEach(name => {
          if (name !== 'constructor' && this.hasMethod(value, name)) {
            names.push(name);
          }
        });
    }

    let proto = Reflect.getPrototypeOf(value);

    while (proto && proto !== stop) {
      Object.getOwnPropertyNames(proto)
        .forEach(name => {
          if (name !== 'constructor' && this.hasMethod(proto, name)) {
            names.push(name);
          }
        });
      proto = Reflect.getPrototypeOf(proto);
    }

    return names;
  }
}

export const classUtils: ClassUtils = new ClassUtils();
